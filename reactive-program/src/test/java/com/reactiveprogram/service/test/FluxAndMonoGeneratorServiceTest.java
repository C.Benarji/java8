package com.reactiveprogram.service.test;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.reactiveprogram.service.FluxAndMonoGeneratorService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class FluxAndMonoGeneratorServiceTest {
	FluxAndMonoGeneratorService fluxAndMonoGeneratorService = new FluxAndMonoGeneratorService();

	@Test
	public void nameFulxTest() {
		// given

		// when
		Flux<String> nameFlux = fluxAndMonoGeneratorService.nameFlux();
		// then
		StepVerifier.create(nameFlux)
				// validate each and every element
				// .expectNext("alex", "bena", "chloe")
				// validate element count
				// .expectNextCount(3)
				.expectNext("alex")// check actual element/event
				.expectNextCount(2)// reaming elements/event count
				.verifyComplete();

	}

	@Test
	public void nameMonoTest() {
		//given
		
		//when
		Mono<String> nameMono = fluxAndMonoGeneratorService.nameMono();
		//then
		StepVerifier.create(nameMono)
					.expectNext("alex")
					.verifyComplete();

	}
	@Test
	public void nameFluxMapTest() {
		//given
		
		//when
		Flux<String> nameFluxMap = fluxAndMonoGeneratorService.nameFluxMap();
		//then
		StepVerifier.create(nameFluxMap)
					.expectNext("ALEX","BENA","CHLOE")
					.verifyComplete();
		
		
	}
	@Test
	public void nameFluxImmutabilityTest() {
		//given
		//when
		Flux<String> nameFlucImmutability = fluxAndMonoGeneratorService.nameFlucImmutability();
		
		//then
		StepVerifier.create(nameFlucImmutability)
		//.expectNext("ALEX","BENA","CHLOE")
		.expectNext("alex", "bena", "chloe")
		.verifyComplete();
	}
	
	@Test
	public void nameFluxApplyFilter() {
		//given
		int stringLength=3;
		//when
		Flux<String> nameFluxIApplyFilter = fluxAndMonoGeneratorService.nameFluxApplyFilter(stringLength);
		
		//then
		StepVerifier.create(nameFluxIApplyFilter)
					.expectNext("4 - ALEX","5 - CHLOE")
					.verifyComplete();
		
	}
	@Test
	public void nameFluxwithFlatMapTest() {
		//given
		int stringLength=3;
		//when
		 Flux<String> nameFluxwithFlatMap = fluxAndMonoGeneratorService.nameFluxwithFlatMap(stringLength);
		
		//then
		StepVerifier.create(nameFluxwithFlatMap)
					.expectNext("A","L","E","X","C","H","L","O","E")
					.verifyComplete();
		
	}
	@Test
	public void nameFluxwithFlatMapAsynchronousTest() {
		//given
		int stringLength=3;
		//when
		 Flux<String> nameFluxwithFlatMap = fluxAndMonoGeneratorService.nameFluxwithFlatMapAsynchronous(stringLength);
		
		//then
		StepVerifier.create(nameFluxwithFlatMap)
					//.expectNext("A","L","E","X","C","H","L","O","E")
					.expectNextCount(9)			
					.verifyComplete();
		
	}
	@Test
	public void nameFluxwithConcatMapTest() {
		//given
		int stringLength=3;
		//when
		 Flux<String> nameFluxwithFlatMap = fluxAndMonoGeneratorService.nameFluxwithConcatMap(stringLength);
		
		//then
		StepVerifier.create(nameFluxwithFlatMap)
					.expectNext("A","L","E","X","C","H","L","O","E")
		//.expectNextCount(9)			
		.verifyComplete();
		
	}
	@Test
	public void nameMonoFlatMapTest() {
		//given
		int stringLength=3;
		//when
		Mono<List<String>> nameMonoFlatMap = fluxAndMonoGeneratorService.nameMonoFlatMap(stringLength);
		//then
		StepVerifier.create(nameMonoFlatMap)
					.expectNext(List.of("A","L","E","X"))
					.verifyComplete();
		
	}

}
