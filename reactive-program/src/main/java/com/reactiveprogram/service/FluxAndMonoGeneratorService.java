package com.reactiveprogram.service;

import java.time.Duration;
import java.util.List;
import java.util.Random;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class FluxAndMonoGeneratorService {

	// Flux deals with Multiple elements
	public Flux<String> nameFlux() {
		return Flux.fromIterable(List.of("alex", "bena", "chloe")) // db or a remote service call
				.log();
	}

	// Mono deals with Single elements
	public Mono<String> nameMono() {
		return Mono.just("alex").log();

	}

	// Each and Every element convert lower case to Upper case
	public Flux<String> nameFluxMap() {
		return Flux.fromIterable(List.of("alex", "bena", "chloe")) // db or a remote service call
				// .map(String::toUpperCase);
				.map(s -> s.toUpperCase()).log();
	}

	// Flux have Immutable Nature
	public Flux<String> nameFlucImmutability() {
		Flux<String> nameFlucImmutability = Flux.fromIterable(List.of("alex", "bena", "chloe"));

		nameFlucImmutability.map(String::toUpperCase).log();
		return nameFlucImmutability;

	}

	public Flux<String> nameFluxApplyFilter(int stringLength) {
		return Flux.fromIterable(List.of("alex", "ben", "chloe"))
				.map(String::toUpperCase)
				.filter(s -> s.length() > stringLength)
				.map(s -> s.length() + " - " + s)// 4-ALEX,5-CHOLE
				.log();
	}

	public Flux<String> nameFluxwithFlatMap(int stringLength) {
		return Flux.fromIterable(List.of("alex", "ben", "chloe"))
				.map(String::toUpperCase)
				.filter(s -> s.length() > stringLength)
				.flatMap(s -> spiltString(s)).log();
	}

	public Flux<String> nameFluxwithFlatMapAsynchronous(int stringLength) {
		return Flux.fromIterable(List.of("alex", "ben", "chloe"))
				.map(String::toUpperCase)
				.filter(s -> s.length() > stringLength)
				.flatMap(s -> spiltStringWithDelay(s))
				.log();
	}

	public Flux<String> nameFluxwithConcatMap(int stringLength) {
		return Flux.fromIterable(List.of("alex", "ben", "chloe"))
				.map(String::toUpperCase)
				.filter(s -> s.length() > stringLength)
				.concatMap(s -> spiltStringWithDelay(s))
				.log();
	}

	public Mono<List<String>> nameMonoFlatMap(int stringLength) {
		return Mono.just("alex").map(String::toUpperCase)
				.filter(s -> s.length() > stringLength)
				.flatMap(this::spiltStringMono)
				.log();

	}

	public Mono<List<String>> spiltStringMono(String name) {
		String[] split = name.split("");
		List<String> list = List.of(split);
		return Mono.just(list);

	}

	public Flux<String> spiltString(String name) {
		String[] split = name.split("");
		return Flux.fromArray(split);

	}

	public Flux<String> spiltStringWithDelay(String name) {
		String[] split = name.split("");
		int delay = new Random().nextInt(1000);
		return Flux.fromArray(split).delayElements(Duration.ofMillis(delay));

	}

	public static void main(String[] args) {
		FluxAndMonoGeneratorService fluxAndMonoGeneratorService = new FluxAndMonoGeneratorService();

	/*	fluxAndMonoGeneratorService.nameFlux().subscribe(name -> { //consume values one by one and print value
			System.out.println("Flux Name is :" + name); //bussies logic
		});*/

		/*fluxAndMonoGeneratorService.nameMono().subscribe(name -> {
			System.out.println("Mono Name is :" + name);
		});*/

		/*fluxAndMonoGeneratorService.nameFluxMap().subscribe(name -> {
			System.out.println("NameFluxMap is :" + name);
		});*/

		/*fluxAndMonoGeneratorService.nameFlucImmutability().subscribe(name -> {
			System.out.println("Name Fluc Immutability: " + name);
		});*/

	/*	fluxAndMonoGeneratorService.nameFluxApplyFilter(3).subscribe(name -> {
			System.out.println("Name FluX Apply Filter : " + name);
		});*/
		/*fluxAndMonoGeneratorService.nameFluxwithFlatMap(3).subscribe(name -> {
			System.out.println("Name FluX  FlatMap : " + name);
		});*/
		/*fluxAndMonoGeneratorService.nameFluxwithFlatMapAsynchronous(3).subscribe(name -> {
			System.out.println("Name Flux with FlatMap Asyn : " + name);
		});*/
		/*fluxAndMonoGeneratorService.nameMonoFlatMap(3).subscribe(name -> {
			System.out.println("Name Mono FlatMap : " + name);
		});*/

	}

}
