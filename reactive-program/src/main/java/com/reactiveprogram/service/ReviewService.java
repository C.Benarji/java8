package com.reactiveprogram.service;

import reactor.core.publisher.Flux;

import java.util.List;

import com.reactiveprogram.model.Review;

public class ReviewService {

    public  List<Review> retrieveReviews(long MovieId){

        var reviewsList = List.of(new Review(MovieId, "Awesome Movie", 8.9),
                new Review(MovieId, "Excellent Movie", 9.0));
        return reviewsList;
    }

    public Flux<Review> retrieveReviewsFlux(long MovieId){

        var reviewsList = List.of(new Review(MovieId, "Awesome Movie", 8.9),
                new Review(MovieId, "Excellent Movie", 9.0));
        return Flux.fromIterable(reviewsList);
    }
}
