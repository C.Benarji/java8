package com.reactiveprogram.imperative;

import java.util.ArrayList;
import java.util.List;

public class ImperativeExample {
	public static void main(String[] args) {
		var nameList = List.of("alex", "ben", "chole", "adam", "adam");
		var newNameList = namesGreaterThanSize(nameList, 3);
		System.out.println(newNameList);
	}

	private static List<String> namesGreaterThanSize(List<String> nameList, int size) {
		var newNamesList = new ArrayList<String>();
		for (String name : nameList) {
			if (name.length() > size && !newNamesList.contains(name)) {
				newNamesList.add(name);

			}

		}
		return newNamesList;
	}
}
