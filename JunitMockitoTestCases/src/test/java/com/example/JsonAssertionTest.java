package com.example;

import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

public class JsonAssertionTest {
	private String actualResp = "{\"id\":1,\"name\":\"pen \",\"price\":2,\"quantity\":100}";

	@Test
	public void jsonAssert_StringTrue_ExactMatchExceptForSpaces() throws JSONException {
		String excepetedResp = "{\"id\": 1,\"name\":\"pen \",\"price\":2,\"quantity\":100}";
		JSONAssert.assertEquals(excepetedResp, actualResp, true);
	}

	@Test
	public void jsonAssert_StringFalse_ExactMatchExceptForSpaces() throws JSONException {
		String excepetedResp = "{\"id\": 1,\"name\":\"pen \",\"price\":2,\"quantity\":100}";
		JSONAssert.assertEquals(excepetedResp, actualResp, false);
	}

	@Test
	public void jsonAssert_WithoutEscapeCharacters() throws JSONException {
		String excepetedResp = "{id: 1,name: \"pen \" ,price :2,quantity:100}";
		JSONAssert.assertEquals(excepetedResp, actualResp, false);
	}

}
