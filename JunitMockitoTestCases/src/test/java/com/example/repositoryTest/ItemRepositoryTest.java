package com.example.repositoryTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.entity.ItemEntity;
import com.example.repository.ItemRepository;

//@RunWith(SpringRunner.class)
//@DataJpaTest
public class ItemRepositoryTest {
	
	@Autowired
	private ItemRepository itemRepository;
	
	//@Test
	public void findAllTest() {
		List<ItemEntity> allItems = itemRepository.findAll();
		assertEquals(3, allItems.size());
	}

}
