package com.example.serviceTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.entity.ItemEntity;
import com.example.repository.ItemRepository;
import com.example.service.impl.ItemServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ItemServiceTest {
	
	@InjectMocks
	private ItemServiceImpl itemService;
	
	@Mock
	private ItemRepository itemRepository;
	
	@Test
	public void getAllItemTest() {
		when(itemRepository.findAll()).thenReturn(Arrays.asList( new ItemEntity(1, "pen", 2, 100, null)));
		List<ItemEntity> allItem = itemService.getAllItem();
		assertEquals(200, allItem.get(0).getValue());
		
	}
	
	

}
