package com.example.controllerTest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.controller.ItemController;
import com.example.entity.ItemEntity;
import com.example.service.ItemService;

@RunWith(SpringRunner.class)
@WebMvcTest(ItemController.class)
public class IteamControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ItemService itemService;
	
	
	

	@Test
	public void getItem() throws Exception {
		MockHttpServletRequestBuilder accept = MockMvcRequestBuilders.get("/admin/getItem")
				.accept(MediaType.APPLICATION_JSON_VALUE);
		when(itemService.getAllItem()).thenReturn(
				Arrays.asList( new ItemEntity(1, "pen", 2, 100, 20)));
		mockMvc.perform(accept).andExpect(status().isOk())
				.andExpect(content().json("[{id:1,name:pen,price:2,quantity:100,value=20}]")).andReturn();

	}

}
