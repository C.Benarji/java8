package com.example.controllerTest;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.controller.TestController;


@RunWith(SpringRunner.class)
@WebMvcTest(TestController.class)
public class TestControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void getTestData() throws Exception {
		MockHttpServletRequestBuilder accept = MockMvcRequestBuilders.get("/get")
				.accept(MediaType.APPLICATION_JSON_VALUE);

		 mockMvc.perform(accept)
				.andExpect(status()
				.isOk())
				.andExpect(content().string("test data is avilibale"))
				.andReturn();
		//assertEquals("test data is avilibale", andReturn.getResponse().getContentAsString());

	}

}
