package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "ITEAM_ENTITY") 
public class ItemEntity { 
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ITEAM_ID")
	private Integer id;
	@Column(name = "ITEM_NAME")
	private String name;
	@Column(name = "ITEAM_PRICE")
	private Integer price;
	@Column(name = "ITEM_QUANTITY")
	private Integer quantity;
	@Transient
	private Integer value;

}
