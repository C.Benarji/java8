package com.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.entity.ItemEntity;
import com.example.model.Item;
import com.example.repository.ItemRepository;
import com.example.service.ItemService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemRepository itemRepository;

	@Override
	public List<ItemEntity> getAllItem() {
		log.info("Enter into getItem......!");
		List<ItemEntity> findAll = itemRepository.findAll();
		for (ItemEntity itemEntity : findAll) {
			itemEntity.setValue(itemEntity.getPrice() * itemEntity.getQuantity());

		}
		log.info("End of getItem......!");
		return findAll;

	}

	@Override
	public ItemEntity saveItem(Item item) {
		log.info("Enter into saveItem.....!");
		ItemEntity entity = new ItemEntity(); 
		entity.setId(item.getId());
		entity.setName(item.getName());
		entity.setPrice(item.getPrice());
		entity.setQuantity(item.getQuantity());
		log.info("End of  saveItem.....!");
		return itemRepository.save(entity);
	}

}
