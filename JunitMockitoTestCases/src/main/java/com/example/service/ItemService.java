package com.example.service;

import java.util.List;

import com.example.entity.ItemEntity;
import com.example.model.Item;

public interface ItemService {

	public List<ItemEntity> getAllItem();

	public ItemEntity saveItem(Item item);

}
