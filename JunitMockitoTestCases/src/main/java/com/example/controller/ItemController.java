package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.ItemEntity;
import com.example.model.Item;
import com.example.service.ItemService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/admin")
@Slf4j
public class ItemController {

	@Autowired
	private ItemService itemService;

	@PostMapping("/save")
	public ResponseEntity<ItemEntity> itemCreate(@RequestBody Item item) {
		log.info("enter into itemCreate...! ");
		ItemEntity saveItem = itemService.saveItem(item);
		log.info("End  of itemCreate...! ");
		return new ResponseEntity<ItemEntity>(saveItem, HttpStatus.CREATED);

	}

	@GetMapping("/getItem")
	public List<ItemEntity> getAllItems() {
		log.info("enter into getAllItems...! ");
		return itemService.getAllItem();

	}

}
