package com.security.service;

import com.security.model.MyAppUser;

public interface UserRegistrationService {

	public void createUser(MyAppUser user);

}
