package com.security.repository;

import org.springframework.data.repository.CrudRepository;

import com.security.model.MyAppUser;

public interface UserRepository extends CrudRepository<MyAppUser, String> {

	public MyAppUser findByUsername(String username);

	public MyAppUser findByEmail(String email);

}
