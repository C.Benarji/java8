package com.security.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.DigestAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.DigestAuthenticationFilter;

@Configuration
@Order(1)
public class AdminSecurityConfiguration extends WebSecurityConfigurerAdapter {

	public DigestAuthenticationEntryPoint getDigestEntryPoint() {
		DigestAuthenticationEntryPoint authenticationEntryPoint = new DigestAuthenticationEntryPoint();
		authenticationEntryPoint.setRealmName("Admin-digest-realm");
		authenticationEntryPoint.setKey("somedigest-realm");
		return authenticationEntryPoint;

	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();

	}

	@Override
	// configure the Authentication Manager
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("benarji").password(passwordEncoder().encode("benarji")).roles("USER")
				.and().withUser("admin").and().withUser("admin").password(passwordEncoder().encode("admin"))
				.roles("ADMIN");
		super.configure(auth);
	}

	@Override
	public UserDetailsService userDetailsServiceBean() throws Exception {
		return super.userDetailsServiceBean();
	}

	// Configure DigestAuthenticationFilter
	// Observe the we are injecting userDetailsService and
	// DigestAuthenticationEntrypoint
	private DigestAuthenticationFilter getDigestAuthFilter() throws Exception {
		DigestAuthenticationFilter authenticationFilter = new DigestAuthenticationFilter();
		authenticationFilter.setUserDetailsService(userDetailsServiceBean());
		authenticationFilter.setAuthenticationEntryPoint(getDigestEntryPoint());
		return authenticationFilter;

	}
	//Observe that we are configure /admin/** for the securityFilterChain configured because of this configure file 
	//Observe how we are customizing the filter chain and adding DigestAuthenticationFilter
	//Also Observe how we are configuring authenticationEntryPoint
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.headers().disable()
			.antMatcher("/admin/**")
			.addFilter(getDigestAuthFilter())
			.exceptionHandling()		
				.authenticationEntryPoint(getDigestEntryPoint())
				.and().authorizeHttpRequests()
				.antMatchers("/admin/**")
				.hasRole("ADMIN");
	}

}
