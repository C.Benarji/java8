package com.camel.sender.routes.kafka;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

//@Component
public class KafkaSender extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("file:files/kafka")
		 .log("${body}")
		  .to("kafka:my-kafka-Topic");
	}

}
