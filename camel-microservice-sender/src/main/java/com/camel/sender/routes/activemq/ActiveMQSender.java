package com.camel.sender.routes.activemq;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

//@Component
public class ActiveMQSender extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		//timer
		from("timer:active-mq-timer?period=60000")
		.transform().constant("My-message for Active MQ")
		.log("${body}")
		//queue
		.to("activemq:my-activemq-queue");
	}

}
