package com.camel.sender.routes.activemq;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

//@Component
public class ActiveMQXmlFileSender extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("file:files/xml")
		.log("${body}")
		.to("activemq:my-activemq-Xml-queue");

	}

}
