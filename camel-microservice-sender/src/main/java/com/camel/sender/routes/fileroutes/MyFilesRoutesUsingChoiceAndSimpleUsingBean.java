package com.camel.sender.routes.fileroutes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.camel.sender.routes.model.DeciderBean;

//@Component
public class MyFilesRoutesUsingChoiceAndSimpleUsingBean extends RouteBuilder {
	
	@Autowired
	private DeciderBean bean;

	@Override
	public void configure() throws Exception {
		from("file:files/Activeinput")
		.transform().body(String.class)		
		.routeId("MyFiles-RoutesUsing-ChoiceAndSimple-custom-bean")
		.choice()
		.when(simple("${file:ext} ends with 'xml'"))		
		    .log("XML FILE...!")
		 .when(method(bean))
		  .log("custom bean NOT An XML FILE but contains USD")
		 
		.otherwise()
		    .log("NOT An XML FILE...!")
	   .end()
	   .to("direct://log-file-bean-values")
	   .to("file:files/Activeoutput");
	   
	   from("direct://log-file-bean-values")
		.log("${body}")
		.log("${messageHistory},${file:absolute.path}")
		.log("${file:name} ${file:name.ext} ${file:name.noext} ${file:onlyname}")
		.log("${file:parent} ${file:size} ${file:length} ${file:modified}")
		.log(" ${body}")
		
		.to("file:files/Activeoutput");
		
	}
}
