package com.camel.sender.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;

//@Component
public class MyFirstTimerRouter extends RouteBuilder {
	
	@Autowired
	private GetCurrentBean getCurrentBean;
	
	@Autowired
	private LogingProcessing processing;

	@Override
	public void configure() throws Exception {
		from("timer:first-microservice-timer")
		//.transform().constant("My Static  Messgae")
		//.transform().constant("My Static  Messgae  "+LocalDate.now())
		.log("${body}")
		.transform().constant("My Constant  Messgae")
		.log("${body}")
		.bean(getCurrentBean)
		.log("${body}")
		.bean(processing)
		.log("${body}")
		.to("log:first-microservice-timer-log");
	}

}

