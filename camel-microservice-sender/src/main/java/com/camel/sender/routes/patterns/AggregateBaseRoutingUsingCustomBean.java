package com.camel.sender.routes.patterns;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import com.camel.sender.routes.model.CurrencyExchange;

@Component
public class AggregateBaseRoutingUsingCustomBean extends RouteBuilder {

	
	@Override
	public void configure() throws Exception {
		from("file:files/Aggregate")
		.unmarshal()
		 .json(JsonLibrary.Jackson,CurrencyExchange.class)
		  .aggregate(simple("${body}"),new ArrayListAggregationStrategy())
		   .completionSize(3).to("log:aggregate-json");
		}

}
