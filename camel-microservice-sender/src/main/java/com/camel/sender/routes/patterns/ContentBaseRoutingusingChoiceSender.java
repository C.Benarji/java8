package com.camel.sender.routes.patterns;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

//@Component
public class ContentBaseRoutingusingChoiceSender extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		/*from("file:files/csv")
			.unmarshal()
				.csv()
					.split(body())
						//.to("log:spilt-csv-file");
					.to("activemq:csv-split-queue");*/
		
		from("file:files/csv")
		.convertBodyTo(String.class)
				.split(body(),",")
					//.to("log:spilt-csv-file");
				.to("activemq:csv-split-queue");
		

	}

}
