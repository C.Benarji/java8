package com.camel.sender.routes.model;

import java.util.Map;

import org.apache.camel.Body;
import org.apache.camel.ExchangeProperties;
import org.apache.camel.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component

public class DeciderBean {
	private final static Logger log=LoggerFactory.getLogger(DeciderBean.class);
	
	public boolean isThisConditionalCheck(@Body String body,
			@Header(value = "") Map<String, String> header,
			@ExchangeProperties Map<String, String> exchangeProperties) {
		log.info("DeciderBean {} {} {}" ,body ,header,exchangeProperties);
		return true;
		
	}

}
