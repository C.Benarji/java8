package com.camel.sender.routes.restapi;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

//@Component
public class RestApiConsumer extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		restConfiguration().host("localhost").port(8000);
		from("timer:rest-api-consumer?period=30000")
		.setHeader("from",()->"USD")
		.setHeader("to",()->"IND")
		.log("${body}")
		.to("rest:get:/currency-exchange/from/{from}/to/{to}")
		.log("${body}");
	}

}
