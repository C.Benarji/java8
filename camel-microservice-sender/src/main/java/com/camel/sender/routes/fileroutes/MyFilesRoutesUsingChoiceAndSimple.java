package com.camel.sender.routes.fileroutes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

//@Component
public class MyFilesRoutesUsingChoiceAndSimple extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		from("file:files/Activeinput")
		.transform().body(String.class)		
		.routeId("MyFiles-RoutesUsing-ChoiceAndSimple")
		.choice()
		.when(simple("${file:ext} ends with 'xml'"))		
		    .log("XML FILE...!")
		 .when(simple("${body} contains  'USD'"))
		  .log("NOT An XML FILE but contains USD")
		 
		.otherwise()
		    .log("NOT An XML FILE...!")
	   .end()
	   .to("direct://log-file-values")
	   .to("file:files/Activeoutput");
	   
	   from("direct://log-file-values")
		.log("${body}")
		.log("${messageHistory},${file:absolute.path}")
		.log("${file:name} ${file:name.ext} ${file:name.noext} ${file:onlyname}")
		.log("${file:parent} ${file:size} ${file:length} ${file:modified}")
		.log(" ${body}")
		
		.to("file:files/Activeoutput");
		
	}

}
