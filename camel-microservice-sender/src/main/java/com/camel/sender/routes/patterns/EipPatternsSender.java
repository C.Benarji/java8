package com.camel.sender.routes.patterns;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

//@Component
public class EipPatternsSender extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		from("timer:multicast?period=60000")
//.pipeline()//1.Default pipe line camel provide
		//2.Multicast 
		.multicast()
		.to("log:multicast1","log:multicast2","log:multicast3","log:multicast4");
		//3.contentBaseRouting 
	}

}
