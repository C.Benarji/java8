package com.camel.sender.routes.activemq;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ActiveMQJsonFileSender extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("file:files/json")
		.log("${body}")
		.to("activemq:my-activemq-files-queue");
	}

}
