package com.camel.sender.routes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

//@Component
public class LogingProcessing {

	private Logger logger = LoggerFactory.getLogger(LogingProcessing.class);

	public void process(String message) {
		logger.info("Simple LoggingProcessingComponent {}", message);

	}

}
