package com.camel.sender.routes.patterns;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContentBaseRoutingusingCustomBeanSender extends RouteBuilder {
	@Autowired
	private SplitterComponent component;

	@Override
	public void configure() throws Exception {
		from("file:files/csv")
			.convertBodyTo(String.class)
				.split(method(component))
					.to("activemq:csv-split-queue");

	}

}
