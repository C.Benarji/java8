package com.example.demo.functionalInterfaces;

import java.util.List;
import java.util.function.Predicate;

public class PredicateNot {

	public static boolean isEven(Integer num) {
		return num % 2 == 0;

	}

	public static void main(String[] args) {
		List<Integer> numbers = List.of(2, 3, 45, 67, 8, 10);
		Predicate<Integer> evenNumberFilter = num -> num % 2 == 0;
		numbers.stream().filter(evenNumberFilter).forEach(System.out::println);
		
		System.out.println("====printing odd numbers using negate()method");
		numbers.stream().filter(evenNumberFilter.negate()).forEach(System.out::println);
		
		System.out.println("====printing Even numbers using method Referance");
		numbers.stream().filter(PredicateNot::isEven).forEach(System.out::println);
		System.out.println("====printing odd numbers using PredicateNot()method");
//Usecase Predicate.not
		//Java11
		// Returns a predicate that is the negation of the supplied predicate.
		// This is accomplished by returning result of the calling
		numbers.stream().filter(Predicate.not(PredicateNot::isEven)).forEach(System.out::println);
	}

}
