package com.example.demo.stringutility;

class Sample {
	String str = null;
}

public class StringNewAPI {

	public static void main(String[] args) {
//UseCase1 isBlank()
		// java11
		// Returns if the string is empty or contains only white space
		System.out.println(" ".isBlank());
//UseCase2: strip()
		// java11
		// Returns a string whose value is this string, with all leading and trailing
		// white space removed.
		System.out.println(" LR ".strip());
//UseCase3:stripLeading()
		// java11
		// Return a string whose value is this string, with all leading white space
		// removed
		System.out.println(" LR ".stripLeading());
//UseCase4:stripTrailing()
		// java11
		// a string whose value is this string, with all trailing white space removed
		System.out.println(" LR ".stripTrailing());
//UseCase5:lines()
		// java11
		// Returns a stream of lines extracted from this string, separated by line
		// terminators.
		"Line1\nLine2\nLine3\nLine4".lines().forEach(System.out::println);
//UseCase6:	transform(write your own function)
		// Java12
		// This method allows the application of a function to string. The function
		// should expect a single String argument
		// and produce an result.
		System.out.println("BENARJI".transform(x -> x.substring(2)));
//UseCase6: formatted(arg1,arg2)
		// Java12
		// Formats using this string as the format string, and the supplied arguments.
		System.out.println("My name is %s and My age is %d".formatted("Benarji", 26));
		Sample sampe = new Sample();
		System.out.println(sampe.str.isBlank());

		// Exception in thread "main" java.lang.NullPointerException
		// at com.example.demo.stringutility.StringNewAPI.main(StringNewAPI.java:44) in
		// java11 but came java14 msg given clearly

	}

}
