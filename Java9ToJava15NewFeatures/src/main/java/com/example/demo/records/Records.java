package com.example.demo.records;

public class Records {

	// java15 record introduced
	/*
	 * Eliminate verbosity in creating Java beans public accessor
	 * method,constructor,equals,hascode and toStringMethod are automatically
	 * Created create custom implementations if you want
	 *Compact Constructors are only allowed in Recods
	 *you can add ststic fileds,static initializers and static methods
	 *But you cannot add instance variables or instance initializers
	 *However you can add instance methods
	 */
	record Person(String name, String email, String PhoneNumber) {
		// custom constructor
		//int num; ->instance variables are not allowed 
		//below one is compact Constructors
		Person {
			// if you want add validations
			/*
			 * if (this.name == null){ throw new IllegalAccessException("Name is not null");
			 * 
			 * }
			 */
			

		}

	}

	public static void main(String[] args) {
		Person person = new Person("Benarji", "xyz@gmail.com", "1234567890");
		Person person1 = new Person("Benarji", "xyz@gmail.com", "1234567890");
		Person person2 = new Person("Benarji1", "xyz@gmail.com", "1234567890");
		System.out.println(person);
		System.out.println(person.name);
		System.out.println(person.equals(person1));
		System.out.println(person.equals(person2));

	}

}
