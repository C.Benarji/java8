package com.example.demo.textblocks;

public class TextBlocks {

	public static void main(String[] args) {
		//these String value is difficult to read
System.out.println("\"First Line\"\nSecond Line\nThird Line");
//Java15 they are introduced TextBlocks
//First Line """ followed by line terminator like
//"""abc or """abc""" -->NOT valid
//Automatic Alignment is done
//Trailing with White Space is Stripped
//you can use text blocks where ever you can use a string
/*System.out.println("""
		First Line
		    Second Line
		       Third Line
				 """");*/
	}

}
