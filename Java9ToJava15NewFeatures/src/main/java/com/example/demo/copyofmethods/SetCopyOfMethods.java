package com.example.demo.copyofmethods;

import java.util.HashSet;
import java.util.Set;

public class SetCopyOfMethods {
	public static void main(String[] args) {
		Set<String> name = new HashSet<>();
		name.add("Ravi");
		name.add("Raju");
		name.add("Ram");
		name.add("Rakesh");
		doNotChange(name);
		System.out.println("existing set  change" + name);
//UsesCase1: 
		// java9 featue Set.of() if you know the static set if you know the exact values
		// Returns an unmodifiable set containing elements then go for set.of() method

		Set<String> of = Set.of("Ravi", "Raju", "Ram", "Rakesh");
		doNotChange(of);
		System.out.println("existing set not change using Set.of Method" + of);
//UsesCase2: 
		// this set of value anything you can Returns an unmodifiable set containing
		// elements
		// java10 featue :Set.copyOf()
		Set<String> copyOf = Set.copyOf(name);
		doNotChange(copyOf);

	}

	private static void doNotChange(Set<String> name) {
		name.add("Should not allow to add");

	}

}
