package com.example.demo.copyofmethods;

import java.util.HashMap;
import java.util.Map;

public class MapCopyOfMethods {

	public static void main(String[] args) {
		Map<String, Integer> name = new HashMap<>();
		name.put("Ravi", 20);
		name.put("Raju", 30);
		name.put("Ram", 40);
		name.put("Rakesh", 50);
		doNotChange(name);
		System.out.println("existing map  change" + name);
//UsesCase1: 
		// java9 featue Map.of() if you know the static map if you know the exact values
		// Returns an unmodifiable map containing elements then go for map.of() method

		Map<String, Integer> of = Map.of("Ravi", 20, "Raju", 30, "Ram", 40, "Rakesh", 50);
		doNotChange(of);
		System.out.println("existing map not change using Map.of() Method" + of);
//UsesCase2: 
		// this map of value anything you can Returns an unmodifiable map containing
		// elements
		// java10 featue :Map.copyOf()
		Map<String, Integer> copyOf = Map.copyOf(name);
		doNotChange(copyOf);

	}

	private static void doNotChange(Map<String, Integer> name) {
		name.put("Should not allow to add", 600);

	}

}
