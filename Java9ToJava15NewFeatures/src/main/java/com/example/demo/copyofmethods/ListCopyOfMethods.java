package com.example.demo.copyofmethods;

import java.util.ArrayList;
import java.util.List;

public class ListCopyOfMethods {
	public static void main(String[] args) {
		List<String> name = new ArrayList<>();
		name.add("Ravi");
		name.add("Raju");
		name.add("Ram");
		name.add("Rakesh");
		
		doNotChange(name);
		System.out.println("existing list  change" + name);
//UsesCase1: 
		// java9 featue List.of() if you know the static list if you know the exact
		// values and
		// Returns an unmodifiable list containing three elements then go for this
		List<String> listOfmethod = List.of("Ravi", "Raju", "Ram", "Rakesh");
		doNotChange(listOfmethod);
//UsesCase2: 
		// this list of value anything you can Returns an unmodifiable list containing
		// elements
		// java10 featue :List.copyOf()
		List<String> copyOfNames = List.copyOf(name);
		doNotChange(copyOfNames);
		System.out.println("Using List.Copy Method" + copyOfNames);

	}

	private static void doNotChange(List<String> name) {
		name.add("should not allow");

	}

}
