package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java9ToJava15NewFeaturesApplication {

	public static void main(String[] args) {
		SpringApplication.run(Java9ToJava15NewFeaturesApplication.class, args);
	}

}
