package com.example.demo.typeinference;

import java.util.List;

public class TypeInference {

	public static void main(String[] args) {
		List<String> list1 = List.of("Benarji", "sachin");
		List<String> list2 = List.of("chandu", "Ramesh");
		List<List<String>> listOfList = List.of(list1, list2);
		System.out.println(listOfList);

		// Java10 TypeInference
		// Java compiler infers the type of the variables at compile time

		var typeInference = List.of(list1, list2);
		typeInference.stream().forEach(System.out::println);
		var newList1 = List.of("Benarji", "sachin");
		var newList2 = List.of("chandu", "Ramesh");
		// and made final also but by defulat not final
		final var finallist = List.of("chandu", "Ramesh");
		// we can also use var in loops
		for (String varlist : finallist) {

			System.out.println(varlist);
		}
		//var not allow to null value Cannot infer type for local variable initialized to 'null'
		//var abc=null;
		//var is not a KEY word  typeinfer is not applicable for member variables
       
	}

}
