package com.example.demo.switchcases;

public class SwitchExpression {

	public static String switchcase(int day) {
		String dayOfWeek = "";
		//In case of switch fallthrough(break is required)
		switch (day) {
		case 0:
			dayOfWeek = "sunday";
			break;
		case 1:
			dayOfWeek = "Monday";
			break;
		case 2:
			dayOfWeek = "Tuesday";
			break;
		case 3:
			dayOfWeek = "Wenday";
			break;
		case 4:
			dayOfWeek = "Thurday";
			break;
		default:
			throw new IllegalArgumentException("Unexpected value: " + day);
		}
		return dayOfWeek;
	}

//Java14
	//need to remember no fallthrough
	public static String switchexpression(int day) {
		String dayOfWeek = switch (day) {

		case 0 -> "sunday";
		// complex business logic then syntax will be change
		case 1 -> {
			System.out.println("Some complex business logic");
			yield "Monday";
		}

		case 2 -> "Tuesday";

		case 3 -> "Wenday";

		case 4 -> "Thurday";

		default -> throw new IllegalArgumentException("Unexpected value: " + day);
		};
		return dayOfWeek;
	}

	public static void main(String[] args) {

	}

}
