package com.example.demo.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileReadingAndFileWritingString {

	public static void main(String[] args) throws IOException {
		// String strClassPath = System.getProperty( "java.class.path" );
		// System.out.println( "Classpath is " + strClassPath);
//UseCase1:
		Path path = Paths.get("src/main/resources/sample.txt");
		System.out.println(path);
		// Java11 Feature: ReadString()
		String fileContent = Files.readString(path);
		System.out.println(fileContent);
//UseCase2:		
		System.out.println("========write opertion======");
		String newfileContent = fileContent.replace("Benarji", "chintapalli Benarji");
		Path newpath = Paths.get("src/main/resources/sample-new.txt");
		// Java11 Feature: Writetring()
		Path newFileWriteString = Files.writeString(newpath, newfileContent);
		System.out.println(newFileWriteString);
	}

}
