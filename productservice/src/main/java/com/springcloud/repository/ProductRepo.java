package com.springcloud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springcloud.entity.Product;

public interface ProductRepo extends JpaRepository<Product, Long> {

}
