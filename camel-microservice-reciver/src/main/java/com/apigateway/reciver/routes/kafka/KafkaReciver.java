package com.apigateway.reciver.routes.kafka;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

//@Component
public class KafkaReciver extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		from("kafka:my-kafka-Topic")
		.to("log:receiver-message-from-kafka");
		
	}

}
