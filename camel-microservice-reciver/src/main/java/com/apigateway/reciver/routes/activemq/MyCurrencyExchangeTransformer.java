package com.apigateway.reciver.routes.activemq;

import java.math.BigDecimal;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.apigateway.reciver.routes.model.CurrencyExchange;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MyCurrencyExchangeTransformer {
	
	
	public CurrencyExchange TransformerMessage(CurrencyExchange currencyExchange) {
		log.info("Do some Transforming with  currencyExchange.setConversionMultiple()  value ");
		currencyExchange.setConversionMultiple(currencyExchange.getConversionMultiple().multiply(BigDecimal.TEN));
		return currencyExchange;

	}

}
