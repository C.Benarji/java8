package com.apigateway.reciver.routes;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ActiveMQReciver extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		from("activemq:my-activemq-queue")
	//	.log("${body}")
		.to("log:received-message-from-active-mq");
	}

}
