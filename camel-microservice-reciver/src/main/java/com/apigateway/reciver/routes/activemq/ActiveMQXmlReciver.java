package com.apigateway.reciver.routes.activemq;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.apigateway.reciver.routes.model.CurrencyExchange;

@Component
public class ActiveMQXmlReciver extends RouteBuilder {
	@Autowired
	private MyCurrencyExchangeProcessor currencyExchangeProcessor;

	@Autowired
	private MyCurrencyExchangeTransformer currencyExchangeTransformer;

	@Override
	public void configure() throws Exception {
		from("activemq:my-activemq-Xml-queue")
		.unmarshal()
		 .jacksonXml(CurrencyExchange.class)
		  .to("log:received-message-from-active-mq-xml-queue");
	}

}
