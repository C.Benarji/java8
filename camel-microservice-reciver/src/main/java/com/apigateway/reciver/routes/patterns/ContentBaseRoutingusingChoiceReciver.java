package com.apigateway.reciver.routes.patterns;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class ContentBaseRoutingusingChoiceReciver extends RouteBuilder{

	@Override
	public void configure() throws Exception {
		from("activemq:csv-split-queue")
			.to("log:received-message-from-activemq");		
	}

}
