package com.apigateway.reciver.routes.activemq;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.apigateway.reciver.routes.model.CurrencyExchange;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MyCurrencyExchangeProcessor {
	
	
	public void ProcessorMessage(CurrencyExchange currencyExchange) {
		log.info("Do some Processing with  currencyExchange.getConversionMultipile()  value "
				+ currencyExchange.getConversionMultiple());

	}

}
