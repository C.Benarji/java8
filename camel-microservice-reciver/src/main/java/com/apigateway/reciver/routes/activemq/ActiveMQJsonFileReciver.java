package com.apigateway.reciver.routes.activemq;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.apigateway.reciver.routes.model.CurrencyExchange;

@Component
public class ActiveMQJsonFileReciver extends RouteBuilder {
	
	@Autowired
	private MyCurrencyExchangeProcessor currencyExchangeProcessor;
	
	@Autowired
	private MyCurrencyExchangeTransformer currencyExchangeTransformer;

	@Override
	public void configure() throws Exception {
		from("activemq:my-activemq-files-queue")
		.unmarshal().json(JsonLibrary.Jackson,CurrencyExchange.class)
		//.bean("currencyExchangeProcessor")
	//	.bean("currencyExchangeTransformer")
		.to("log:received-message-from-active-mq-file-queue");
	}

}
