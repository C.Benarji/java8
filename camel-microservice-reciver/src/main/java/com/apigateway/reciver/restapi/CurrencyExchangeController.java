package com.apigateway.reciver.restapi;

import java.math.BigDecimal;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.apigateway.reciver.routes.model.CurrencyExchange;

@RestController
public class CurrencyExchangeController {

	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public CurrencyExchange frindConversionValue(@PathVariable String from, @PathVariable String to) {
		return new CurrencyExchange(1000L, from,to, BigDecimal.TEN);

	}

}
