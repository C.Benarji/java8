package com.currencyconversion.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.currencyconversion.entity.currencyconversion;
import com.currencyconversion.service.CurrencyConversionService;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CurrencyConversionController {

	@Autowired
	private CurrencyConversionService conversionService;

	@GetMapping("/currency-conversion/from/{from}/to/{to}/quantity/{quantity}")
	public currencyconversion calculateCurrencyConversion(@PathVariable String from, @PathVariable String to,
			@PathVariable BigDecimal quantity) {
		log.info("Enter into calculateCurrencyConversion "+getClass());
		currencyconversion retriveCurrencyConversionFromAndTo = conversionService
				.retriveCurrencyConversionFromAndTo(from, to, quantity);
		log.info("End of calculateCurrencyConversion "+getClass());
		return retriveCurrencyConversionFromAndTo;
	}

}
