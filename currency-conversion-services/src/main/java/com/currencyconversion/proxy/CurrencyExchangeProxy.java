package com.currencyconversion.proxy;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.currencyconversion.entity.currencyconversion;

//@FeignClient(url = "localhost:8000",name = "currency-exchange")
@FeignClient(name = "currency-exchange")
public interface CurrencyExchangeProxy {

	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	public currencyconversion retrieveExchangeValues(@PathVariable String from, @PathVariable String to);

}
