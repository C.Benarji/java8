package com.currencyconversion.service.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.currencyconversion.entity.currencyconversion;
import com.currencyconversion.proxy.CurrencyExchangeProxy;
import com.currencyconversion.service.CurrencyConversionService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CurrencyConversionServiceImpl implements CurrencyConversionService {
	

	@Autowired
	private CurrencyExchangeProxy currencyExchangeProxy;

	@Override
	public currencyconversion retriveCurrencyConversionFromAndTo(String from, String to, BigDecimal quantity) {
		log.info("Enter into retriveCurrencyConversionFromAndTo "+getClass());
		System.out.println(quantity);
		currencyconversion retrieveExchangeValues = currencyExchangeProxy.retrieveExchangeValues(from, to);
		retrieveExchangeValues.getConversionMultiple();
		currencyconversion currencyconversion = retrieveExchangeValues;
		quantity.multiply(currencyconversion.getConversionMultiple());
		currencyconversion.setTotalCalculatedAmount(quantity.multiply(currencyconversion.getConversionMultiple()));
		currencyconversion.setQuantity(quantity);
		log.info("End of retriveCurrencyConversionFromAndTo "+getClass());
		return currencyconversion;
	}

}
