package com.currencyconversion.service;

import java.math.BigDecimal;

import com.currencyconversion.entity.currencyconversion;

public interface CurrencyConversionService {

	public currencyconversion retriveCurrencyConversionFromAndTo(String from, String to, BigDecimal quantity);

}
