package com.apigateway.configuration;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class APIGatewayConfiguration {

	//@Bean
	public RouteLocator gatewayRouter(RouteLocatorBuilder builder) {
		
		/*
		 * .filters(f->f .addRequestHeader("MyHeader","MyHeader"))
		 * .uri("http://httpbin.org:80");
		 */
		/*
		 * return builder.routes() .route(p->p.path("/get") .filters(f->f
		 * .addRequestHeader("MyHeader", "MyURI") .addRequestParameter("Param",
		 * "MyValue")) .uri("http://httpbin.org.80"))
		 * .route(p->p.path("/currency-exchange/**") .uri("lb://currency-exchange"))
		 * .build();
		 */
		return builder.routes()
				.route(p-> p.path("/get")
				.filters(f->f.addRequestHeader("myHeadre", "Hello")
						.addRequestParameter("Param", "MyValue"))
				.uri("http://httpbin.org:80"))
		.route(p->p.path("/currency-exchange/**") 
				.uri("lb://currency-exchange"))
		.route(p->p.path("/currency-conversion/**")
				.uri("lb://currency-conversion"))
				.build();
	}

}
