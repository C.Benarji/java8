package com.currency.exchane.services.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.currency.exchane.services.entity.CurrencyExchange;
import com.currency.exchane.services.repository.CurrencyExchangeRepository;
import com.currency.exchane.services.service.CurrencyExchangeService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {

	@Autowired
	private CurrencyExchangeRepository repository;

	@Autowired
	private Environment environment;

	@Override
	public List<CurrencyExchange> getAll() {
		log.info("Enter into getAll Method" + getClass());
		return repository.findAll();
	}

	@Override
	public CurrencyExchange findByFromAndTo(String from, String to) {
		log.info("Enter into getAll findByFromAndTo" + getClass());

		CurrencyExchange findByFromAndTo = repository.findByFromAndTo(from, to);
		environment.getProperty("local.server.port");
		if (findByFromAndTo == null) {
			log.debug("Exception  is occures while " + findByFromAndTo);
			throw new RuntimeException("Unable to find" + from + " and " + to);

		}
		log.info("End of getAll findByFromAndTo" + getClass());
		return findByFromAndTo;
	}

	@Override
	public CurrencyExchange save(CurrencyExchange currencyExchange) {
		log.info("Enter into save method" + getClass());
		return repository.save(currencyExchange);

	}

}
