package com.currency.exchane.services.controller;

import java.util.concurrent.CompletableFuture;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.resilience4j.timelimiter.annotation.TimeLimiter;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class TimeLimiterController {

	/*
	 * What is Time Limiting or Timeout Handling? 
	 * Time Limiting is the process of setting a time limit for a Microservice to respond. Suppose Microservice ‘A’
	 * sends a request to Microservice ‘B’, it sets a time limit for the
	 * Microservice ‘B’ to respond. If Microservice ‘B’ doesn’t respond within that
	 * time limit, then it will be considered that it has some fault.
	 */

	@GetMapping("/getMessageTL")
	@TimeLimiter(name = "getMessageTL")
	public CompletableFuture<String> getMessageTimeLimiter() {
		return CompletableFuture.supplyAsync(this::getResponse);
	}

	private String getResponse() {

		if (Math.random() < 0.4) { // Expected to fail 40% of the time
			return "Executing Within the time Limit...";
		} else {
			try {
				log.info("Getting Delayed Execution");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return "Exception due to Request Timeout.";
	}
}
