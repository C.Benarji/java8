package com.currency.exchane.services.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.currency.exchane.services.entity.CurrencyExchange;
import com.currency.exchane.services.service.CurrencyExchangeService;

import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class CurrencyExchangeController {

	@Autowired
	private CurrencyExchangeService currencyExchangeService;

	@GetMapping("/currency-exchange/from/{from}/to/{to}")
	@Retry(name = "retry-currency-exchange")
	public CurrencyExchange retrieveExchangeValues(@PathVariable String from, @PathVariable String to) {
		log.info("Enter into retrieveExchangeValues" + getClass());

		CurrencyExchange findByFromAndTo = currencyExchangeService.findByFromAndTo(from, to);
	//	new RestTemplate().getForEntity("http://localhost:8999/dumy", String.class);
		log.info("End of the retrieveExchangeValues" + getClass());
		return findByFromAndTo;
	}

	@PostMapping("/save")
	public CurrencyExchange create(@RequestBody CurrencyExchange currencyExchange) {
		log.info("Enter into retrieveExchangeValues" + getClass());
		return currencyExchangeService.save(currencyExchange);

	}
}
