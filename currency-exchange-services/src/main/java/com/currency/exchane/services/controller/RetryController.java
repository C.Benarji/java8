package com.currency.exchane.services.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import io.github.resilience4j.retry.annotation.Retry;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class RetryController {
	
	/*What is Retry?
	 * Suppose Microservice ‘A’  depends on another Microservice ‘B’. 
	 * Let’s assume Microservice ‘B’ is a faulty service and its success rate is only upto 50-60%. 
	 * However, fault may be due to any reason, such as service is unavailable, buggy service that 
	 * sometimes responds and sometimes not, or an intermittent network failure etc. However, 
	 * in this case, if Microservice ‘A’ retries to send request 2 to 3 times, the chances of getting response increases
	 */
	
	@GetMapping("/getInvoiceRetry")
	@Retry(name = "getInvoiceRetry", fallbackMethod = "getInvoiceFallback")
	public String getInvoiceRetry() {
		log.info("getInvoice() call starts here"+getClass());
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> entity = restTemplate.getForEntity("http://localhost:8080/invoice/rest/find/2",
				String.class);
		log.info("Response :" + entity.getStatusCode());
		return entity.getBody();
	}

	public String getInvoiceFallback(Exception e) {
		log.info("---RESPONSE FROM FALLBACK METHOD---");
		return "SERVICE IS DOWN, PLEASE TRY AFTER SOMETIME !!!";
	}

}
