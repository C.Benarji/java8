package com.currency.exchane.services.service;

import java.util.List;

import com.currency.exchane.services.entity.CurrencyExchange;

public interface CurrencyExchangeService {

	public List<CurrencyExchange> getAll();
	
	public CurrencyExchange  findByFromAndTo(String from, String to);

	public CurrencyExchange save(CurrencyExchange currencyExchange);

}
