package com.currency.exchane.services.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class BulkheadController {
	
	
	/* What is Bulkhead?
	 * In the context of the Fault Tolerance mechanism, if we want to limit the number of concurrent requests, 
	 * we can use Bulkhead as an aspect. Using Bulkhead, we can limit the number of concurrent requests within 
	 * a particular period
	 */
	
	@GetMapping("/getMessage")
	@Bulkhead(name = "getMessageBH", fallbackMethod = "getMessageFallBack")
	public ResponseEntity<String> getMessageBulkhead(@RequestParam(value = "name", defaultValue = "Hello") String name) {
		log.info("Enter into getMessage method from "+getClass().getName());

		return ResponseEntity.ok().body("Message from getMessage() :" + name);
	}

	public ResponseEntity<String> getMessageFallBack(RequestNotPermitted exception) {

		log.info("Bulkhead has applied, So no further calls are getting accepted");

		return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS)
				.body("Too many requests : No further request will be accepted. Plese try after sometime");
	}

}
