package com.example.demo.imperativeDeclerative;

public class RunnableImplemantation {
	public static void main(String[] args) {
		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				int sum = 0;
				for (int i = 0; i < 10; i++) {
					sum += i;
					System.out.println("Traditonal way " + sum);
				}
				System.out.println("Traditonal way " + sum);
			}

		};
		new Thread(runnable).start();

		// implemetation using lambadaExpression

		Runnable runnable2 = () -> {
			int sum = 0;
			for (int i = 0; i < 5; i++) {
				sum += i;
				System.out.println("Implemetation using lambadaExpression" + sum);

			}
			System.out.println("Implemetation using lambadaExpression" + sum);

		};
		new Thread(runnable2).start();

		new Thread(() -> {
			int sum = 0;
			for (int i = 0; i < 6; i++) {
				sum += i;

			}
			System.out.println("inside thread lambada Expression implemanation" + sum);
		}).start();

	}

}
