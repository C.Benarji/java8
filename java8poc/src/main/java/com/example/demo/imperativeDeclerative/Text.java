package com.example.demo.imperativeDeclerative;

import java.util.stream.IntStream;

public class Text {
	public static void main(String[] args) {
		//IntStream s = IntStream.of(1, 2, 3, 4, 5, 6);
		int s = IntStream.range(1, 6).sum();
		System.out.print(s);
	}

}
