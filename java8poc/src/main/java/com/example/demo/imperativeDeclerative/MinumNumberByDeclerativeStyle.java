package com.example.demo.imperativeDeclerative;

import java.util.Arrays;
import java.util.List;

public class MinumNumberByDeclerativeStyle {
	public static void main(String[] args) {
		List<Integer> list= Arrays.asList(-1,2,3,4,0,5,6,7,8,9);
		list.stream().min(Integer::compareTo);
		System.out.println("min value"+list.stream().min(Integer::compareTo));
		System.out.println("max value "+list.stream().max(Integer::compareTo));
	}

}
