package com.example.demo.imperativeDeclerative;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

public class SumOfNumbersUsingCallableWithLambada {
	public static int[] arry = IntStream.rangeClosed(0, 5000).toArray();
	public static int total = IntStream.rangeClosed(0, 5000).sum();

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		Callable callable1 = () -> {
			int sum = 0;
			for (int i = 0; i < arry.length / 2; i++) {
				sum = sum + arry[i];

			}
			return sum;
		};
		Callable callable2 = () -> {
			int sum = 0;
			for (int i = arry.length / 2; i < arry.length; i++) {
				sum = sum + arry[i];

			}
			return sum;
		};
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		List<Callable<Integer>> taskList = Arrays.asList(callable1, callable2);
		List<Future<Integer>> invokeAll = executorService.invokeAll(taskList);

		int k = 0;
		int sum = 0;
		for (Future<Integer> future : invokeAll) {
			sum = sum + future.get();
			System.out.println("sum of" + ++k + "is " + future.get());
		}
		System.out.println("sum from callable  " + sum);
		System.out.println("sum from IntTSream " + total);
		executorService.shutdown();

	}

}
