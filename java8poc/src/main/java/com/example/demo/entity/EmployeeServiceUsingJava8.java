package com.example.demo.entity;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class EmployeeServiceUsingJava8 {
	public static void main(String[] args) {

		List<Employee> list = Arrays.asList(new Employee(1, "Benarji", 25, "Male", "IT", 2020, 55000),
				new Employee(2, "Chandu", 27, "Male", "IT", 2020, 50000.0),
				new Employee(3, "Ramesh", 29, "Male", "RND", 2021, 70000),
				new Employee(4, "Ramya", 29, "Female", "RND", 2021, 80000));
		list.stream().count();
		System.out.println(list.stream().count());
		Map<String, Long> noOfMaleAndFemaleEmployees = list.stream()
				.collect(Collectors.groupingBy(Employee::getGender, Collectors.counting()));
		System.out.println("noOfMaleAndFemaleEmployees :" + noOfMaleAndFemaleEmployees);
		list.stream().map(Employee::getDepartment).distinct().forEach(System.out::println);
		Map<String, Double> avgAgeOfMaleAndFemaleEmployees = list.stream()
				.collect(Collectors.groupingBy(Employee::getGender, Collectors.averagingInt(Employee::getAge)));
		System.out.println("avgAgeOfMaleAndFemaleEmployees : " + avgAgeOfMaleAndFemaleEmployees);
		Optional<Employee> highestPaidEmployeeWrappe = list.stream()
				.collect(Collectors.maxBy(Comparator.comparingDouble(Employee::getSalary)));
		System.out.println("highestPaidEmployeeWrappe : " + highestPaidEmployeeWrappe.get().getSalary());
		list.stream().filter(e -> e.getYearOfJoining() > 2015).map(Employee::getName).forEach(System.out::println);
		Map<String, Long> employeeCountByDepartment = list.stream()
				.collect(Collectors.groupingBy(Employee::getDepartment, Collectors.counting()));
		System.out.println("employeeCountByDepartment : " + employeeCountByDepartment);
		Map<String, Double> avgSalaryOfDepartments = list.stream().collect(
				Collectors.groupingBy(Employee::getDepartment, Collectors.averagingDouble(Employee::getSalary)));

		Set<Entry<String, Double>> entrySet = avgSalaryOfDepartments.entrySet();
		for (Entry<String, Double> entry : entrySet) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
			Optional<Employee> youngestMaleEmployeeInITDepartment = list.stream()
					.filter(e -> e.getGender().equals("Male") && e.getDepartment().equals("IT"))
					.min(Comparator.comparingInt(Employee::getAge));
			System.out.println(
					"youngestMaleEmployeeInITDepartment : " + youngestMaleEmployeeInITDepartment.get().getName());

		}

	}
}