package com.example.demo.functionalInterfaces;

@FunctionalInterface
public interface FunInterfaces {
	
	public String funInterFace();
	
	public default String defaultMethod() {
		return " Default Method ";
	}
	public static String staticMethod() {
		return "Static Method";
		
	}

}
