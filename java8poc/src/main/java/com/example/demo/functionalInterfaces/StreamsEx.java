package com.example.demo.functionalInterfaces;

import java.util.Arrays;
import java.util.List;

public class StreamsEx {
	public static void main(String[] args) {
		
		List<Person> list=Arrays.asList(new Person(1,"Benarji","Hyd",15000),  
				                         new Person(2,"Ramesh","Hyd",25000),
				                         new Person (3,"Chandu","Hyd",45000));
	list.stream().filter(person -> person.getId() > 1).forEach(System.out::println);
	list.stream().filter(person ->person.getSalary() > 25000).forEach(System.out::println);
				                         
	}
	
	

}
