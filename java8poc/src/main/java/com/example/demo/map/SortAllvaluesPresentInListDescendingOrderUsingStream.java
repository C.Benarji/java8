package com.example.demo.map;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SortAllvaluesPresentInListDescendingOrderUsingStream {

	public static void main(String[] args) {
		List<Integer> existingList = Arrays.asList(33, 3, 4, 5, 80, 1, 55, 44, 66, 11, 1, 22, 66, 2);
		existingList.stream().sorted(Collections.reverseOrder()).forEach(System.out::println);

	}

}
