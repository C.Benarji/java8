package com.example.demo.map;

import java.util.Arrays;
import java.util.List;

public class FilterMethod {
	/*
	  filter method is used to filter elements that satisfy a certain condition
	  that specify using predicate function
	 */
	
	public static void main(String[] args) {
		List<Integer> listOfInterger = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		
		Integer integer = listOfInterger.stream()
		 .filter(i -> i % 2 == 0)
		 .filter(i -> i % 2 != 0)
		  .findFirst()
		  .get();
		System.out.println(integer);
	}
	

}
