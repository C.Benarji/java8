package com.example.demo.map;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FindFirstNonRepeatedCharacterUsingStream {
	// https://javahungry.blogspot.com/2013/12/first-non-repeated-character-in-string-java-program-code-example.html
	public static void main(String[] args) {
		/*
		 * String str="hello java and springboot";
		 * str.chars().mapToObj(s->Character.toLowerCase(Character.valueOf((char)s)))///
		 * / First convert to Character object and then to lowercase
		 * .collect(Collectors.groupingBy(Function.identity(),LinkedHashMap::new,
		 * Collectors.counting()))////Store the chars in map with count ;
		 */
		String str = "hello java and springboot";
		Map<Character, Integer> map = new LinkedHashMap<>();
		for (Character ch : str.toCharArray()) {
			map.put(ch, map.containsKey(ch) ? map.get(ch) + 1 : 1);

		}
		Character nonRepeatedCharacter = map.entrySet().stream().filter(x -> x.getValue() == 1).findFirst().get()
				.getKey();
		System.out.println(nonRepeatedCharacter);
	}

}
