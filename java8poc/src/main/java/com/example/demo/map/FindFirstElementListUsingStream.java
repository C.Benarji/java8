package com.example.demo.map;

import java.util.Arrays;
import java.util.List;

public class FindFirstElementListUsingStream {
	public static void main(String[] args) {
		List<Integer> existingList = Arrays.asList(10, 20, 30, 40, 50, 60, 70, 80);
		existingList.stream().findFirst().ifPresent(System.out::println);

	}
}
