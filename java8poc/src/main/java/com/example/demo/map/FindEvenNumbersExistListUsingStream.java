package com.example.demo.map;

import java.util.Arrays;
import java.util.List;

public class FindEvenNumbersExistListUsingStream {
	public static void main(String[] args) {
		List<Integer> existinglist = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		existinglist.stream().filter(i -> i % 2 == 0).forEach(System.out::println);
	}

}
