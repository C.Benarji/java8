package com.example.demo.map;

import java.util.Arrays;
import java.util.List;

public class FindMaximumValueElementPresentUsingStream {
	public static void main(String[] args) {
		List<Integer> existingList = Arrays.asList(1, 2, 3, 44, 55, 667, 88, 99, 232);
		Integer max = existingList.stream().max(Integer::compareTo).get();
		System.out.println(max);
		Integer min = existingList.stream().min(Integer::compareTo).get();
		System.out.println(min);

	}

}
