package com.example.demo.map;

import java.util.Arrays;
import java.util.List;

public class FindAllNumbersStartingWith1UsingStream {
	public static void main(String[] args) {
		List<Integer> existingList=Arrays.asList(1,10,20,14,17,30,27,41,15);
		existingList.stream().map(s->s+"") //coverting String
		//.filter(s->s.startsWith("1"))
		.filter(s->s.endsWith("0"))
		.forEach(System.out::println);
		
	}

}
