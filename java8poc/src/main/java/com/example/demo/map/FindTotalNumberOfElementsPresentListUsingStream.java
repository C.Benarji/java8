package com.example.demo.map;

import java.util.Arrays;
import java.util.List;

public class FindTotalNumberOfElementsPresentListUsingStream {
	public static void main(String[] args) {
		List<Integer> existingList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 7, 8);
		long count = existingList.stream().count();
		System.out.println(count);

	}

}
