package com.example.demo.map;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FindDuplicateElementsGivenIntegersListUsingStream {
	public static void main(String[] args) {
		List<Integer> exisitngList= Arrays.asList(1,1,2,3,4,5,6,5,8,5,3,8,9,0);
		//exisitngList.stream().distinct().forEach(System.out::println); //one-way
		Set<Integer> set= new HashSet();
		exisitngList.stream().filter(n->set.add(n))
		.forEach(System.out::println);//2nd way
		
	}

}
