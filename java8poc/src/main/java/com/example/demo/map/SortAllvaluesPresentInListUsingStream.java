package com.example.demo.map;

import java.util.Arrays;
import java.util.List;

public class SortAllvaluesPresentInListUsingStream {

	public static void main(String[] args) {
		List<Integer> existingList = Arrays.asList(2, 5, 1, 7, 10, 50, 6, 55, 34, 90, 0);
		existingList.stream().sorted().forEach(System.out::println);

	}

}
