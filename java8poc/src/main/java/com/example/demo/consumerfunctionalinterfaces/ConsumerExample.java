package com.example.demo.consumerfunctionalinterfaces;

import java.util.function.Consumer;

public class ConsumerExample {

	public static void main(String[] args) {
		Consumer<String> c = (x) -> System.out.println(x.length());
		c.accept("Helo consumer functional interfaces");

		// implementation block statement
		Consumer<Integer> cn = (x) -> {
			System.out.println("x*x := " + x * x);
			System.out.println("x/x := " + x / x);
		};
		cn.accept(20);
	}
}
