package com.example.demo.consumerfunctionalinterfaces;

import java.util.function.BiConsumer;

public class BiConsumerExample {

	public static void main(String[] args) {
		BiConsumer<Integer, Integer> biConsumer = (x, y) -> System.out.println(+x + " :" + " " + y);
		biConsumer.accept(20, 50);

		BiConsumer<String, String> stringBiConsumer = (x, y) -> System.out.println(x + y);
		stringBiConsumer.accept("hello", "Java ");

	}

}
