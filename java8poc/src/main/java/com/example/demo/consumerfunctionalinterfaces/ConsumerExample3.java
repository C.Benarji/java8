package com.example.demo.consumerfunctionalinterfaces;

import java.util.function.DoubleConsumer;
import java.util.function.IntConsumer;
import java.util.function.LongConsumer;

public class ConsumerExample3 {
	public static void main(String[] args) {
		IntConsumer intConsumer = (a) -> System.out.println(a * 20);
		intConsumer.accept(10);

		DoubleConsumer doubleConsumer = (a) -> System.out.println(a * 10.30);
		doubleConsumer.accept(20.75);

		LongConsumer longConsumer = (a) -> System.out.println(a * 5);
		longConsumer.accept(6);
	}

}
