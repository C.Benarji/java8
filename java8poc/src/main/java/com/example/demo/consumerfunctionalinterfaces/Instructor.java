package com.example.demo.consumerfunctionalinterfaces;

import java.util.List;

public class Instructor {
	private String name;
	private Integer yearsOfExp;
	private String title;
	private Boolean onlineCourse;
	private List<String> courses;

	public Instructor(String name, Integer yearsOfExp, String title, Boolean onlineCourse, List<String> courses) {
		super();
		this.name = name;
		this.yearsOfExp = yearsOfExp;
		this.title = title;
		this.onlineCourse = onlineCourse;
		this.courses = courses;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getYearsOfExp() {
		return yearsOfExp;
	}

	public void setYearsOfExp(Integer yearsOfExp) {
		this.yearsOfExp = yearsOfExp;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Boolean getOnlineCourse() {
		return onlineCourse;
	}

	public void setOnlineCourse(Boolean onlineCourse) {
		this.onlineCourse = onlineCourse;
	}

	public List<String> getCourses() {
		return courses;
	}

	public void setCourses(List<String> courses) {
		this.courses = courses;
	}

	@Override
	public String toString() {
		return "Instructor [name=" + name + ", yearsOfExp=" + yearsOfExp + ", title=" + title + ", onlineCourse="
				+ onlineCourse + ", courses=" + courses + "]";
	}

}
