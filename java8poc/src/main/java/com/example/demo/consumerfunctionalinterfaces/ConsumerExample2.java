package com.example.demo.consumerfunctionalinterfaces;

import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample2 {

	public static void main(String[] args) {
		List<Instructor> instructor = Instructors.getAll();
		// looping through all instructor and print out the values of instructor
		Consumer<Instructor> c1 = (x) -> System.out.println(x);
		instructor.forEach(c1);

		// looping through all instructor and print out their names
		Consumer<Instructor> c2 = (x) -> System.out.println(x.getName());
		instructor.forEach(c2);

		// looping through all instructor and print out their names and courses
		Consumer<Instructor> c3 = (x) -> System.out.println(x.getCourses());
		instructor.forEach(c2.andThen(c3));

		// looping through all instructor and print out their names and if years of exp
		// is>5
		instructor.forEach(s1 -> {
			if (s1.getYearsOfExp() > 5) {
				c1.accept(s1);

			}
		});

		// looping through all instructor and print out their names and if years of exp
		// is>5 and tecach online
		System.out.println("===========");
		instructor.forEach(s1 -> {
			if (s1.getYearsOfExp() > 5 && s1.getOnlineCourse()) {
				c1.andThen(c2).accept(s1);

			}
		});

	}

}
