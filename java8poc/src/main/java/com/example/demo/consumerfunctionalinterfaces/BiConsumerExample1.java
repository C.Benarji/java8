package com.example.demo.consumerfunctionalinterfaces;

import java.util.List;
import java.util.function.BiConsumer;

public class BiConsumerExample1 {

	public static void main(String[] args) {
		List<Instructor> instructors = Instructors.getAll();
		// printing out name and gender of instructor

		BiConsumer<String, List<String>> biConsumer = (name, courses) -> System.out
				.println("name is : " + name + "  Courses is : " + courses);
		instructors.forEach(Instructor -> biConsumer.accept(Instructor.getName(), Instructor.getCourses()));

		BiConsumer<String, Integer> biConsumer2 = (name, yearsOfExp) -> System.out
				.println("name is : " + name + " year of Exp is : " + yearsOfExp);
		instructors.forEach(Instructor -> biConsumer2.accept(Instructor.getName(), Instructor.getYearsOfExp()));

		BiConsumer<String, Boolean> biConsumer3 = (name, onlineCourse) -> System.out
				.println("name is : " + name + "  Teach OnlineCourses : " + onlineCourse);
		instructors.forEach(Instructor -> biConsumer3.accept(Instructor.getName(), Instructor.getOnlineCourse()));
		System.out.println("=================onlineCourse is True =====================");
		instructors.forEach(Instructor -> {
			if (Instructor.getOnlineCourse().equals(Boolean.TRUE)) {
				biConsumer2.accept(Instructor.getName(), Instructor.getYearsOfExp());

			}
		});
	}

}
