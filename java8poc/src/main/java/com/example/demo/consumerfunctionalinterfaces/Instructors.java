package com.example.demo.consumerfunctionalinterfaces;

import java.util.Arrays;
import java.util.List;

public class Instructors {

	public static List<Instructor> getAll() {

		Instructor instructor1 = new Instructor("Benarji", 10, "Software Developer", true,
				Arrays.asList("java", "springboot", "Hibernate", "Microservices"));

		Instructor instructor2 = new Instructor("chandu", 3, "Software Developer", false,
				Arrays.asList("java", "springboot", "mulesoft", "Microservices"));

		Instructor instructor3 = new Instructor("Ramesh", 17, "Software Developer", true,
				Arrays.asList("java", "Devops", "CI/CD", "Microservices"));

		Instructor instructor4 = new Instructor("vamsi", 9, "Software Developer", false,
				Arrays.asList("java", "springboot", "Hibernate", "RestApi"));
		Instructor instructor5 = new Instructor("mani", 5, "Software Developer", false,
				Arrays.asList("java", "Angular", "Hibernate", "Microservices", "ReactJs"));

		List<Instructor> list = Arrays.asList(instructor1, instructor2, instructor3, instructor4, instructor5);

		return list;

	}

}
