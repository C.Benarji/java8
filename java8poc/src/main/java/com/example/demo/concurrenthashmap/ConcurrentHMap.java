package com.example.demo.concurrenthashmap;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHMap {
	public static void main(String[] args) {
		ConcurrentHashMap<Integer, String> map = new ConcurrentHashMap<>();
		map.put(100, "Benarji");
		map.put(200, "Chintapalli");
		map.remove(200);
		System.out.println(map);

		map.putIfAbsent(200, "xyz");
		System.out.println(map);
		map.replace(200, "xyz", "Ch");
		System.out.println(map);
	}

}
