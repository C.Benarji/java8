package com.springcloud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springcloud.entity.Coupon;

public interface CouponRepo extends JpaRepository<Coupon, Long> {

	Coupon findByCode(String code);

}
